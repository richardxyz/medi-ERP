package io.renren.modules.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

import java.math.BigDecimal;

/**
 * 物品表
 *
 * @author wangzhenyue wgznye@163.com
 * @since 1.0.0 2020-09-07
 */
@Data
@ApiModel(value = "物品表")
public class GoodsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "物品ID")
	private Integer gid;

	@ApiModelProperty(value = "")
	private String uniquecode;

	@ApiModelProperty(value = "申请人")
	private Integer uid;

	@ApiModelProperty(value = "物品名称")
	private String goodsname;

	@ApiModelProperty(value = "型号")
	private String modelnumber;

	@ApiModelProperty(value = "物品类型ID")
	private Integer gtid;

	@ApiModelProperty(value = "物品规格：个、箱、盒等")
	private String specifications;

	@ApiModelProperty(value = "物品数量(按规格上算）")
	private Integer numofgoods;

	@ApiModelProperty(value = "是否借用;1代表借用；2代表领用")
	private Integer borrowstate;

	@ApiModelProperty(value = "单价")
	private BigDecimal unitprice;

	@ApiModelProperty(value = "项目名称")
	private String entryname;

	@ApiModelProperty(value = "是否在库,1》在库；2》借出；3》出库；4》丢失；5》损坏")
	private String existstate;

	@ApiModelProperty(value = "借用次数")
	private Integer borrowtimes;

	@ApiModelProperty(value = "物品存放货柜位置")
	private Integer gpid;

	@ApiModelProperty(value = "入库时间")
	private Date storetime;

	@ApiModelProperty(value = "出库时间")
	private Date delivertime;

	@ApiModelProperty(value = "哪些部门可以借用或领用")
	private String deptborrow;

	@ApiModelProperty(value = "成本（按照规格最小单位去算成本：每个、每箱等）")
	private BigDecimal costprice;

	@ApiModelProperty(value = "备注")
	private String explains;

	@ApiModelProperty(value = "条形码对应的编码，生成规则：gid-入库时间(例:180601)")
	private String barcode;

	@ApiModelProperty(value = "是否赔付？1》原价赔偿，2不用赔偿")
	private String compensate;

	@ApiModelProperty(value = "最后一次修改时间")
	private Date utime;


}