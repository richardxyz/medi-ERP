package io.renren.modules.goods.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 物品表
 *
 * @author wangzhenyue wgznye@163.com
 * @since 1.0.0 2020-09-07
 */
@Data
public class GoodsExcel {
    @Excel(name = "物品ID")
    private Integer gid;
    @Excel(name = "")
    private String uniquecode;
    @Excel(name = "申请人")
    private Integer uid;
    @Excel(name = "物品名称")
    private String goodsname;
    @Excel(name = "型号")
    private String modelnumber;
    @Excel(name = "物品类型ID")
    private Integer gtid;
    @Excel(name = "物品规格：个、箱、盒等")
    private String specifications;
    @Excel(name = "物品数量(按规格上算）")
    private Integer numofgoods;
    @Excel(name = "是否借用;1代表借用；2代表领用")
    private Integer borrowstate;
    @Excel(name = "单价")
    private BigDecimal unitprice;
    @Excel(name = "项目名称")
    private String entryname;
    @Excel(name = "是否在库,1》在库；2》借出；3》出库；4》丢失；5》损坏")
    private String existstate;
    @Excel(name = "借用次数")
    private Integer borrowtimes;
    @Excel(name = "物品存放货柜位置")
    private Integer gpid;
    @Excel(name = "入库时间")
    private Date storetime;
    @Excel(name = "出库时间")
    private Date delivertime;
    @Excel(name = "哪些部门可以借用或领用")
    private String deptborrow;
    @Excel(name = "成本（按照规格最小单位去算成本：每个、每箱等）")
    private BigDecimal costprice;
    @Excel(name = "备注")
    private String explains;
    @Excel(name = "条形码对应的编码，生成规则：gid-入库时间(例:180601)")
    private String barcode;
    @Excel(name = "是否赔付？1》原价赔偿，2不用赔偿")
    private String compensate;
    @Excel(name = "最后一次修改时间")
    private Date utime;

}