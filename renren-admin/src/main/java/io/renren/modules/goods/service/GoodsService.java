package io.renren.modules.goods.service;


import io.renren.common.service.CrudService;
import io.renren.modules.goods.dto.GoodsDTO;
import io.renren.modules.goods.entity.GoodsEntity;

/**
 * 物品表
 *
 * @author wangzhenyue wgznye@163.com
 * @since 1.0.0 2020-09-07
 */
public interface GoodsService extends CrudService<GoodsEntity, GoodsDTO> {

}