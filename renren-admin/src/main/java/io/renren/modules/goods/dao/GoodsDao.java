package io.renren.modules.goods.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.goods.entity.GoodsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 物品表
 *
 * @author wangzhenyue wgznye@163.com
 * @since 1.0.0 2020-09-07
 */
@Mapper
public interface GoodsDao extends BaseDao<GoodsEntity> {
	
}