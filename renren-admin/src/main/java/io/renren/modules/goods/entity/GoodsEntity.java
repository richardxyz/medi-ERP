package io.renren.modules.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 物品表
 *
 * @author wangzhenyue wgznye@163.com
 * @since 1.0.0 2020-09-07
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("mw_goods")
public class GoodsEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 物品ID
     */
	private Integer gid;
    /**
     * 
     */
	private String uniquecode;
    /**
     * 申请人
     */
	private Integer uid;
    /**
     * 物品名称
     */
	private String goodsname;
    /**
     * 型号
     */
	private String modelnumber;
    /**
     * 物品类型ID
     */
	private Integer gtid;
    /**
     * 物品规格：个、箱、盒等
     */
	private String specifications;
    /**
     * 物品数量(按规格上算）
     */
	private Integer numofgoods;
    /**
     * 是否借用;1代表借用；2代表领用
     */
	private Integer borrowstate;
    /**
     * 单价
     */
	private BigDecimal unitprice;
    /**
     * 项目名称
     */
	private String entryname;
    /**
     * 是否在库,1》在库；2》借出；3》出库；4》丢失；5》损坏
     */
	private String existstate;
    /**
     * 借用次数
     */
	private Integer borrowtimes;
    /**
     * 物品存放货柜位置
     */
	private Integer gpid;
    /**
     * 入库时间
     */
	private Date storetime;
    /**
     * 出库时间
     */
	private Date delivertime;
    /**
     * 哪些部门可以借用或领用
     */
	private String deptborrow;
    /**
     * 成本（按照规格最小单位去算成本：每个、每箱等）
     */
	private BigDecimal costprice;
    /**
     * 备注
     */
	private String explains;
    /**
     * 条形码对应的编码，生成规则：gid-入库时间(例:180601)
     */
	private String barcode;
    /**
     * 是否赔付？1》原价赔偿，2不用赔偿
     */
	private String compensate;
    /**
     * 最后一次修改时间
     */
	private Date utime;
}