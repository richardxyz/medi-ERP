<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>认证中心</title>
</head>

<body class="sign_body">
<div class="container form-margin-top">
    <form action="/renren-admin/login" method="post">
        <h2 class="form-signin-heading" align="center">统一认证系统</h2>
        <p>
        <input type="text" name="username" class="form-control form-margin-top" placeholder="账号" required autofocus>
        </p>
        <p>
        <input type="password" name="password" class="form-control" placeholder="密码" required>
        </p>
        <p>
        <button class="btn btn-lg btn-primary btn-block" type="submit">sign in</button>
        </p>
    </form>
</div>
</body>
</html>
